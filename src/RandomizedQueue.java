import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item>{

    private Item[] array;
    private int n;

    //construct an empty queue
    public RandomizedQueue(){
        array = (Item[]) new Object[2];
    }
    // is the queue empty?
    public boolean isEmpty(){
        return n == 0;
    }
    //return the size of the queue
    public int size(){
        return n;
    }
    //add an item to the back of a queue
    public void enqueue(Item item){

        if (item == null)
            throw new NullPointerException("Cannot add null to queue.");

        if (n == array.length)
            resize(2*array.length);

        array[n++] = item;
    }
    //remove and return an item uniformly at random
    public Item dequeue(){

        if (isEmpty())
            throw new NoSuchElementException("Cannot dequeue from an empty " +
                    "queue.");

        //assume that the array is always contiguous from 0 to n-1
        int r = StdRandom.uniform(0, n);
        Item temp = array[r];

        //plug the empty hole so that the array remains contiguous
        if (r != n-1)
            array[r] = array[n-1];

        array[--n] = null;

        //if the elements only occupy 1/4 of the array, shrink it size n
        //Note: array is always a multiple of 2
        if (n == array.length/4)
            resize(array.length/4);

        return temp;
    }
    private void resize(int capacity){

        if (capacity < 0)
            throw new IllegalArgumentException("Cannot resize to a negative " +
                    "length.");

        if (capacity == 0)
            capacity = 2;

        Item[] newArray = (Item[]) new Object[capacity];

        for (int i = 0; i < n; i++)
            newArray[i] = array[i];

        array = newArray;
    }
    //pick an item uniformly at random from the queue and return it
    public Item sample(){

        if (isEmpty())
            throw new NoSuchElementException("Cannot sample from an empty" +
                    " queue.");

        int r = StdRandom.uniform(0, n);

        return array[r];
    }
    //return an independent iterator
    public Iterator<Item> iterator(){return new RandomizedQueueIterator();}

    private class RandomizedQueueIterator implements Iterator<Item>{

        Item[] random;
        int current = 0;

        public RandomizedQueueIterator(){

            random = (Item[]) new Object[n];

            //shuffle then copy
            for (int i = 0; i < n; i++){
                random[i] = array[i];
                int r = StdRandom.uniform(i+1);
                swap(i, r);
            }
        }

        private void swap(int i, int r){

            Item temp = random[i];
            random[i] = random[r];
            random[r] = temp;
        }
        
        public boolean hasNext(){
            return current < n;
        }
        public Item next(){

            if (!hasNext())
                throw new NoSuchElementException("No elements left to return.");

            return random[current++];
        }
        public void remove(){
            throw new UnsupportedOperationException("The remove operation is" +
                    "not supported.");
        }
    }//RandomizedQueueIterator
}//RandomizedQueue

