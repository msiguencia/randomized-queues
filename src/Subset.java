public class Subset{

    public static void main(String[] args){
        //subset size
        int k = Integer.parseInt(args[0]);
        //number of strings read
        int m = 0;
        RandomizedQueue<String> randomQ = new RandomizedQueue<String>();

        while ( !StdIn.isEmpty() && ++m <= k )
            randomQ.enqueue( StdIn.readString() );

        while ( !StdIn.isEmpty() && k != 0 ){

            String s = StdIn.readString();
            double r = StdRandom.uniform();
            m++;

            //probability of a string being in the subset
            double p = (double) k / m;

            //the probability of picking p exactly is unlikely, therefore use
            //the interval from [0, p] to decide if a string should enter the
            //queue.
            if (r <= p){
                randomQ.dequeue();
                randomQ.enqueue(s);
            }

        }

        while ( !randomQ.isEmpty() && k != 0 )
            System.out.println( randomQ.dequeue() );

    }
}
